#!/bin/bash

echo "正在关闭NSS引擎"
echo "/etc/httpd/conf.d/nss.conf ===> NssEngine off"
sed -i 's/NSSEngine on/# NSSEngine on/g' /etc/httpd/conf.d/nss.conf

flag=true

while $flag; do
  flag=false
  echo "========= WEB =========="
  echo "|| 1. 初始化配置文件   ||"
  echo "|| 2. 添加WEB目录      ||"
  echo "|| 3. 添加WEB用户      ||"
  echo "========================"
  # shellcheck disable=SC2034
  read -p "请选择模式:" mode
  if [ "$mode" == "1" ]; then
    echo "Init ==> /etc/httpd/conf/httpd.conf"
    cp -rf /root/Desktop/_httpd.conf /etc/httpd/conf/httpd.conf
  fi
  if [ "$mode" == "2" ]; then
    read -p "请输入根目录地址:" rootDir
    echo "New ===> ${rootDir}/index.html"
    mkdir -p "${rootDir}"
    chmod 755 "$rootDir"
    touch "${rootDir}/index.html"
    echo "<!-- ${rootDir}/index.html --!>" >"${rootDir}/index.html"
    # 另开一个窗口编辑 index.html
    gnome-terminal -t "在此编辑${rootDir}/index.html" -x bash -c "gedit ${rootDir}/index.html"
    read -p "请输入监听的端口:" port
    # shellcheck disable=SC2034
    read -p "请输入域名:" domain
    echo "Listen $port
NameVirtualHost $(ifconfig -a | grep inet | grep -v 127.0.0.1 | grep -v inet6 | awk '{print $2}' | tr -d "addr:"):$port
<VirtualHost $(ifconfig -a | grep inet | grep -v 127.0.0.1 | grep -v inet6 | awk '{print $2}' | tr -d "addr:"):$port>
#    ServerAdmin webmaster@dummy-host.example.com
     DocumentRoot $rootDir
#    ServerName $domain
#    ErrorLog logs/dummy-host.example.com-error_log
#    CustomLog logs/dummy-host.example.com-access_log common
     DirectoryIndex index.html
</VirtualHost>" >>/etc/httpd/conf/httpd.conf
    # 另开一个窗口编辑
    gnome-terminal -t "请检查/etc/httpd/conf/httpd.conf" -x bash -c "gedit /etc/httpd/conf/httpd.conf"
  fi
  if [ "$mode" == "3" ]; then
    # shellcheck disable=SC2034
    read -p "请输入用户名:" user_name
    useradd -s /sbin/nologin "$user_name"
    passwd sales_manager
    read -p "请输入个人用户主页根目录(/home/\${用户名}/xxx):" user_root_html
    mkdir -p "/home/$user_name/$user_root_html"
    chmod 755 "/home/$user_name/$user_root_html"
    touch "/home/$user_name/$user_root_html/index.html"
    echo "<!-- /home/$user_name/$user_root_html/index.html --!>" >"/home/$user_name/$user_root_html/index.html"
    # 另开一个窗口编辑 index.html
    gnome-terminal -t "在此编辑$user_name的个人主页" -x bash -c "gedit /home/$user_name/$user_root_html/index.html"
    sed -i 's/UserDir disable/# UserDir disable/g' /etc/httpd/conf/httpd.conf
    sed -i "s/#UserDir public_html/UserDir ${user_root_html}/g" /etc/httpd/conf/httpd.conf
  fi
  # shellcheck disable=SC2034
  read -p "是否继续操作(Y/N):" flagStr
  if [ "$flagStr" == "Y" -o "$flagStr" == "yes" ]; then
    flag=true
    clear
  else
    echo "启动web服务: service httpd restart"
    service httpd restart
    echo "按任意键关闭窗口..."
    read -n 1
    echo "进程结束"
  fi
done
