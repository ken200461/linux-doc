#!/bin/bash

read -p "请输入静态IP地址:" ip
read -p "请输入网关地址:" gate

echo "DEVICE=eth0
ONBOOT=yes
BOOTPROTO=none
HWADDR=$(ifconfig eth0 | grep HWaddr | cut -d " " -f 11)
IPADDR=${ip}
GATEWAY=${gate}
" >/etc/sysconfig/network-scripts/ifcfg-eth0

echo "/etc/sysconfig/network-scripts/ifcfg-eth0 ===> ${ip}"

echo "正在重启网卡: service network restart"

service network restart

echo "执行: ifconfig eth0"

ifconfig eth0

echo "按任意键关闭窗口..."
read -n 1
echo "进程结束"
