#!/bin/bash

flag=true

while $flag; do
  flag=false
  echo "========= FTP =========="
  echo "|| 1. 还原配置文件     ||"
  echo "|| 2. 修改FTP配置文件  ||"
  echo "|| 3. 添加FTP用户      ||"
  echo "========================"

  read -p "请选择模式:" mode

  if [ "$mode" == "1" ]; then
    echo "正在还原配置文件"
    echo "Init ===> /etc/vsftpd/vsftpd.conf"
    cp -rf /root/Desktop/_vsftpd.con:gf /etc/vsftpd/vsftpd.conf
  fi

  if [ "$mode" == "2" ]; then
    echo "正在初始化配置文件"
    echo "Init ===> /etc/vsftpd/vsftpd.conf"
    cp -rf /root/Desktop/_vsftpd.conf /etc/vsftpd/vsftpd.conf
    read -p "请输入ftp根目录路径:" local_root
    if [ ! -f "$local_root" ]; then
      mkdir -p "$local_root"
      echo "New ===> $local_root"
    fi
    sed -i -e "s:local_root=.*:local_root=$local_root:g" /etc/vsftpd/vsftpd.conf
    read -p "请输入根目录($local_root)的所有者，通常与ftpAdmin相同:" local_root_owner
    chown "$local_root_owner" "$local_root"
    read -p "是否允许本地用户写文件(Y/N):" write_enable
    if [ "$write_enable" == "Y" -o "$write_enable" == "yes" ]; then
      sed -i -e "s:write_enable=.*:write_enable=YES:g" /etc/vsftpd/vsftpd.conf
    else
      sed -i -e "s:write_enable=.*:write_enable=NO:g" /etc/vsftpd/vsftpd.conf
    fi
    read -p "文件上传后是否需要修改目录所有者(Y/N):" chown_uploads
    if [ "$chown_uploads" == "Y" -o "$chown_uploads" == "yes" ]; then
      sed -i -e "s:chown_uploads=.*:chown_uploads=YES:g" /etc/vsftpd/vsftpd.conf
    else
      sed -i -e "s:chown_uploads=.*:chown_uploads=NO:g" /etc/vsftpd/vsftpd.conf
    fi
    read -p "是否限制带宽(Y/N):" local_max_rate_flag
    if [ "$local_max_rate_flag" == "Y" -o "$local_max_rate_flag" == "yes" ]; then
      read -p "限制带宽 (b/s):" local_max_rate
      sed -i -e "s:#*\s*local_max_rate=.*:local_max_rate=$local_max_rate:g" /etc/vsftpd/vsftpd.conf
    else
      sed -i -e "s:^local_max_rate=.*:# local_max_rate=300000:g" /etc/vsftpd/vsftpd.conf
    fi

    read -p "是否允许匿名用户(Y/N):" anonymous_enable
    if [ "$anonymous_enable" == "Y" -o "$anonymous_enable" == "yes" ]; then
      sed -i -e "s:anonymous_enable=.*:anonymous_enable=YES:g" /etc/vsftpd/vsftpd.conf
      read -p "是否赋予匿名用户上传权限(Y/N):" anon_upload_enable
      if [ "$anon_upload_enable" == "Y" -o "$anon_upload_enable" == "yes" ]; then
        sed -i -e "s:anon_upload_enable=.*:anon_upload_enable=YES:g" /etc/vsftpd/vsftpd.conf
      else
        sed -i -e "s:anon_upload_enable=.*:anon_upload_enable=NO:g" /etc/vsftpd/vsftpd.conf
      fi
      read -p "是否赋予匿名用户修改文件夹权限(Y/N):" anon_other_write_enable
      if [ "$anon_other_write_enable" == "Y" -o "$anon_other_write_enable" == "yes" ]; then
        sed -i -e "s:anon_other_write_enable=.*:anon_other_write_enable=YES:g" /etc/vsftpd/vsftpd.conf
      else
        sed -i -e "s:anon_other_write_enable=.*:anon_other_write_enable=NO:g" /etc/vsftpd/vsftpd.conf
      fi
      read -p "是否赋予匿名用户创建文件夹权限(Y/N):" anon_mkdir_write_enable
      if [ "$anon_mkdir_write_enable" == "Y" -o "$anon_mkdir_write_enable" == "yes" ]; then
        sed -i -e "s:anon_mkdir_write_enable=.*:anon_mkdir_write_enable=YES:g" /etc/vsftpd/vsftpd.conf
      else
        sed -i -e "s:anon_mkdir_write_enable=.*:anon_mkdir_write_enable=NO:g" /etc/vsftpd/vsftpd.conf
      fi
      read -p "是否赋予匿名用户文件下载权限(Y/N):" anon_world_readable_only
      if [ "$anon_world_readable_only" == "Y" -o "$anon_world_readable_only" == "yes" ]; then
        sed -i -e "s:anon_world_readable_only=.*:anon_world_readable_only=YES:g" /etc/vsftpd/vsftpd.conf
      else
        sed -i -e "s:anon_world_readable_only=.*:anon_world_readable_only=NO:g" /etc/vsftpd/vsftpd.conf
      fi
    else
      sed -i -e "s:anonymous_enable=.*:anonymous_enable=NO:g" /etc/vsftpd/vsftpd.conf
      sed -i -e "s:anon_upload_enable=.*:anon_upload_enable=NO:g" /etc/vsftpd/vsftpd.conf
      sed -i -e "s:anon_other_write_enable=.*:anon_other_write_enable=NO:g" /etc/vsftpd/vsftpd.conf
      sed -i -e "s:anon_mkdir_write_enable=.*:anon_mkdir_write_enable=NO:g" /etc/vsftpd/vsftpd.conf
      sed -i -e "s:anon_world_readable_only=.*:anon_world_readable_only=NO:g" /etc/vsftpd/vsftpd.conf
    fi

    read -p "是否将用户限止在自己的home目录(Y/N):" chroot_local_user
    if [ "$chroot_local_user" == "Y" -o "$chroot_local_user" == "yes" ]; then
      sed -i -e "s:chroot_local_user=.*:chroot_local_user=YES:g" /etc/vsftpd/vsftpd.conf
    else
      sed -i -e "s:chroot_local_user=.*:chroot_local_user=NO:g" /etc/vsftpd/vsftpd.conf
    fi
    read -p "是否允许例外情况(Y/N):" chroot_list_enable
    if [ "$chroot_list_enable" == "Y" -o "$chroot_list_enable" == "yes" ]; then
      sed -i -e "s:chroot_list_enable=.*:chroot_list_enable=YES:g" /etc/vsftpd/vsftpd.conf
      touch /etc/vsftpd/chroot_list
      # 清空文件
      echo "# 在此编辑例外情况" >/etc/vsftpd/chroot_list
      gnome-terminal -t "在此编辑例外情况" -x bash -c "gedit /etc/vsftpd/chroot_list"
    else
      sed -i -e "s:chroot_list_enable=.*:chroot_list_enable=NO:g" /etc/vsftpd/vsftpd.conf
    fi

    d_list="白"
    read -p "是否启用用户黑名单，单独编辑白名单(Y/N):" userlist_deny
    if [ "$userlist_deny" == "Y" -o "$userlist_deny" == "yes" ]; then
      sed -i -e "s:userlist_deny=.*:userlist_deny=YES:g" /etc/vsftpd/vsftpd.conf
      # d_list="白"
    else
      sed -i -e "s:userlist_deny=.*:userlist_deny=NO:g" /etc/vsftpd/vsftpd.conf
      d_list="黑"
    fi
    read -p "是否启用用户${d_list}名单(Y/N):" userlist_enable
    if [ "$userlist_enable" == "Y" -o "$userlist_enable" == "yes" ]; then
      sed -i -e "s:userlist_enable=.*:userlist_enable=YES:g" /etc/vsftpd/vsftpd.conf
      touch /etc/vsftpd/user_list
      # 清空文件
      echo "# 在此编辑用户${d_list}名单" >/etc/vsftpd/user_list
      gnome-terminal -t "在此编辑用户${d_list}名单" -x bash -c "gedit /etc/vsftpd/user_list"
    else
      sed -i -e "s:userlist_enable=.*:userlist_enable=NO:g" /etc/vsftpd/vsftpd.conf
    fi
    echo "Change ===> /etc/vsftpd/vsftpd.conf"
    echo "配置成功"

    echo "Change ===> /etc/hosts.allow"
    echo "vsftpd:$(ifconfig -a | grep inet | grep -v 127.0.0.1 | grep -v inet6 | awk '{print $2}' | tr -d "addr:")
vsftpd:all:DENY" >/etc/hosts.allow
  fi

  if [ "$mode" == "3" ]; then
    read -p "请输入用户名:" user_name
    useradd -s /sbin/nologin "$user_name"
    passwd "$user_name"
    read -p "对于上传的文件，其所有者自动设置为$user_name(Y/N):" ftpAdmin
    if [ "$ftpAdmin" == "Y" -o "$ftpAdmin" == "yes" ]; then
      sed -i -e "s:chown_username=.*:chown_username=$user_name:g" /etc/vsftpd/vsftpd.conf
      echo "Change ===> chown_username=$user_name"
    fi
  fi

  read -p "是否继续操作(Y/N):" flagStr
  if [ "$flagStr" == "Y" -o "$flagStr" == "yes" ]; then
    flag=true
    clear
  else
    echo "启动ftp服务: service vsftpd restart"
    service vsftpd restart
    echo "按任意键关闭窗口..."
    read -n 1
    echo "进程结束"
  fi
done
