#!/bin/bash

echo "警告！请检查区域文件，确保没有重复配置再继续执行！"
read -p "是否需要重置全部DNS配置(Y/N):" b
read -p "请输入你的学号(两位):" num
read -p "请输入IP的最后一位(192.168.\${学号}.xx):" ip
read -p "请输入你需要解析的根域名(baidu.com):" domain
read -p "请输入你需要解析的三级域名(www/dns/ftp):" third

# 判断是否需要初始化
if [ ! -f "/var/named/chroot/var/named/localhost.zone" -o "$b" == "Y" -o "$b" == "yes" ]; then
  rm -rf /var/named/chroot/var/named
  mkdir -p /var/named/chroot/var/named/data
  mkdir -p /var/named/chroot/var/named/slaves
  echo "正在初始化配置文件"
  echo "/usr/share/doc/bind-9.3.3/sample/etc/*.* ===> /var/named/chroot/etc"
  cp /usr/share/doc/bind-9.3.3/sample/etc/*.* /var/named/chroot/etc
  echo "/usr/share/doc/bind-9.3.3/sample/var/named/*.* ===> /var/named/chroot/var/named"
  cp /usr/share/doc/bind-9.3.3/sample/var/named/*.* /var/named/chroot/var/named
fi

echo "正在配置区域文件"
echo "Overwrite ===> /var/named/chroot/etc/named.conf"
echo "options
{
    query-source    port 53;
    query-source-v6 port 53;

    directory \"/var/named\";
    dump-file         \"data/cache_dump.db\";
    statistics-file     \"data/named_stats.txt\";
    memstatistics-file     \"data/named_mem_stats.txt\";
};
include \"/etc/named.rfc1912.zones\";
" >/var/named/chroot/etc/named.conf

sleep .5

echo "Append ===> /var/named/chroot/etc/named.rfc1912.zones"
echo "
zone \"${domain}\" IN {
    type master;
    file \"${domain}.zone\";
    allow-update { none; };
};

zone \"${num}.168.192.in-addr.arpa\" IN {
    type master;
    file \"${domain}.arp\";
    allow-update { none; };
};" >>/var/named/chroot/etc/named.rfc1912.zones

sleep .5

echo "正在配置正向解析文件"
echo "192.168.${num}.${ip} ===> ${third}.${domain}"

# 判断是否存在文件
if [ ! -f "/var/named/chroot/var/named/${domain}.zone" ]; then
  echo "New ===> /var/named/chroot/var/named/${domain}.zone"
  echo "\$TTL    86400
@        IN SOA    @       root (
                    42        ; serial (d. adams)
                    3H        ; refresh
                    15M        ; retry
                    1W        ; expiry
                    1D )        ; minimum

@                IN NS            ${third}.${domain}.
${third}.${domain}.    IN A            192.168.${num}.${ip}
" >"/var/named/chroot/var/named/${domain}.zone"
else
  echo "Append ===> /var/named/chroot/var/named/${domain}.zone"
  echo "@                IN NS            ${third}.${domain}.
${third}.${domain}.    IN A            192.168.${num}.${ip}
" >>"/var/named/chroot/var/named/${domain}.zone"
fi

sleep .8

echo "正在配置逆向解析文件"
echo "${third}.${domain} ===> ${ip}"

# 判断是否存在文件
if [ ! -f "/var/named/chroot/var/named/${domain}.arp" ]; then
  echo "New ===> /var/named/chroot/var/named/${domain}.arp"
  echo "\$TTL    86400
@       IN      SOA     localhost. root.localhost.  (
                                      1997022700 ; Serial
                                      28800      ; Refresh
                                      14400      ; Retry
                                      3600000    ; Expire
                                      86400 )    ; Minimum
@       IN      NS      ${third}.${domain}.
${ip}.${num}.168.192.in-addr.arpa. IN PTR ${third}.${domain}.
" >"/var/named/chroot/var/named/${domain}.arp"
else
  echo "Append ===> /var/named/chroot/var/named/${domain}.arp"
  echo "@       IN      NS      ${third}.${domain}.
${ip}.${num}.168.192.in-addr.arpa. IN PTR ${third}.${domain}.
" >>"/var/named/chroot/var/named/${domain}.zone"
fi

sleep .8

echo "Append ===> /etc/resolv.conf"
echo "
domain ${third}.${domain}
nameserver 192.168.${num}.${ip}
" >>/etc/resolv.conf

echo "${domain}的正逆向解析已全部配置完成！！！"

echo "正在启动DNS服务: service named restart"

service named restart

echo "按任意键关闭窗口..."
read -n 1
echo "进程结束"
