# 搭建ftp服务器

## 课堂练习

> 本案例中静态IP为 `192.168.${学号}.10`

**搭建一台简单的FTP服务器，允许所有员工对服务器上的特定目录`/var/ftp/mypub_${姓名}`进行上传、下载和重命名文件，并且允许创建目录。 <br/>
对于上传的文件，其所有者自动设置为`ftpadmin_${姓名}`。**

### 1. 创建系统用户

```shell
useradd -s /sbin/nologin ftpadmin_${姓名}
passwd ftpadmin_${姓名}
```

### 2. 创建目录并修改权限

```shell
mkdir /var/ftp/mypub_${姓名}
chown ftp:ftp /var/ftp/mypub_${姓名}
# 开放全部权限
chmod 777 /var/ftp/mypub_${姓名}
# 查看目录权限
ls -l /var/ftp
```

### 3. 修改配置文件

> /etc/vsftpd/vsftpd.conf

```shell
# 根目录
local_root=/var/ftp/mypub_${姓名}
# 允许匿名用户
anonymous_enable=YES
# 允许本地用户
local_enable=YES
write_enable=YES
# 匿名用户上传文件
anon_upload_enable=YES
# 控制匿名用户对文件和文件夹的删除和重命名
anon_other_write_enable=YES
# 控制匿名用户对文件夹的创建权限
anon_mkdir_write_enable=YES
# 控制匿名用户对文件的下载权限
anon_world_readable_only=NO
# 文件上传后允许修改目录所有者
chown_uploads=YES
# 将目录的所有者修改为 ftpadmin_${姓名}
chown_username=ftpadmin_${姓名}
local_umask=022
dirmessage_enable=YES
xferlog_enable=YES
connect_from_port_20=YES
xferlog_std_format=YES
listen=YES
pam_service_name=vsftpd
tcp_wrappers=YES
```

### 4. 修改网络（IE）设置

* 不进行这一步将无法使用图形化界面使用ftp服务
* 关闭下图所指的选项

![IE1](../img/ftp/ie1.png)
![IE2](../img/ftp/ie2.png)

### 5. 使用ftp服务

* 启动服务：`service vsftpd start`
* 使用**我的文档**访问目录：`ftp://192.168.${学号}.10`

```shell
# 使用终端访问目录
ftp 192.168.${学号}.10
# 上传文件
put ${文件路径}
# 创建文件夹
mkdir newDir
# 删除文件
delete ${文件名}
```

**有一台FTP和Web服务器FTP服务器主要用于维护公司的网站，包括上传文件、创建目录、更新网页等。**
**公司现有两个部门负责维护任务，分别使用`user1_${姓名}`和`user2_${姓名}`账号进行管理。**
**将它们登录FTP的根目录限制为`/var/www/html_${姓名}`，不能进入任何其他目录。**

### 6. 创建ftp账号

```shell
useradd -s /sbin/nologin user1_${姓名}
useradd -s /sbin/nologin user2_${姓名}
# 设置密码
passwd user1_${姓名}
passwd user2_${姓名}
```

### 7. 创建ftp根目录

```shell
mkdir /var/www/html_${姓名}
# 其他用户可读可写
chmod 777 /var/www/html_${姓名}
```

### 8. 再次修改配置文件

> /etc/vsftpd/vsftpd.conf

```shell
anonymous_enable=NO
# 本地用户允许登录
local_enable=YES
# 本地用户可写
write_enable=YES
# 根目录
local_root=/var/www/html_${姓名}
# 是否将用户限止在自己的home目录
chroot_local_user=NO
# 是否允许例外情况
chroot_list_enable=YES
# 例外情况配置文件
chroot_list_file=/etc/vsftpd/chroot_list
local_umask=022
dirmessage_enable=YES
xferlog_enable=YES
connect_from_port_20=YES
xferlog_std_format=YES
listen=YES
pam_service_name=vsftpd
userlist_enable=YES
tcp_wrappers=YES
```

### 9. 修改chroot_list配置文件

> /etc/vsftpd/chroot_list

```shell
user1_${姓名}
user2_${姓名}

# 重启ftp服务
service vsftpd restart
```

### 10. 验证效果🍻

```shell
ftp 192.168.${学号}.10
# 使用 ftp 用户
# 发现登录失败
# 切换用户
user user1_${姓名}
# user user2_${姓名}
# 创建子目录
mkdir newDir
# 上传文件
put ${文件路径}
# 检查当前目录
pwd
# 更改目录
cd ../
# 发现更改失败
pwd
```

## 拓展练习

> 本案例中静态IP为 `192.168.${学号}.8`

**配置一台FTP服务器，其IP地址是`192.168.${学号}.8`，子网掩码是`255.255.255.0`，使用默认端口21。**
**创建目录`/tmp/${姓名缩写}`，目录里包含文件`hello_zs.txt`。<br/>要求：**

1. 禁止匿名用户访问这个目录。
2. 设置只允许`${姓名缩写}`和`user_test`用户访问，其他用户不能访问，需要验证第三个用户。
3. 只允许以`${姓名缩写}`对该目录有全部的控制权（包括创建子目录，建立、删除、修改文件、上传文件等），`user_test`用户只能下载。
4. 自己和user_test用户不能改变所登录的目录。
5. 将下载的最大带宽控制在300KB/s。

### 1. 创建目录、文件及用户

```shell
# 创建目录和文件
mkdir /tmp/${姓名缩写}
cd /tmp/${姓名缩写}
touch hello_zs.txt
# 创建用户
useradd -s /sbin/nologin ${姓名缩写}
useradd -s /sbin/nologin user_test
# 设置密码
passwd ${姓名缩写}
passwd user_test
```

### 2. 修改目录权限

```shell
# 将目录的所有者设置为 ${姓名缩写}
chown ${姓名缩写} /tmp/${姓名缩写}
# 查看目录的所有者
ls -l /tmp
```

### 3. 修改配置文件

> /etc/vsftpd/vsftpd.conf

```shell
# 禁止匿名用户访问
anonymous_enable=NO
# 本地用户允许登录
local_enable=YES
# 本地用户可写
write_enable=YES
# 根目录
local_root=/tmp/${姓名缩写}
# 本地用户可以改变登录后的根目录
chroot_local_user=NO
# 登录后根目录的限制
chroot_list_enable=YES
# chroot 配置文件
chroot_list_file=/etc/vsftpd/chroot_list
# 启用用户访问控制列表
userlist_enable=YES
# 启用白名单
userlist_deny=NO
# 启用黑名单
# userlist_deny=YES
userlist_file=/etc/vsftpd/user_list
# 限制带宽 (b/s)
# 300000b = 300kb
local_max_rate=300000
local_umask=022
dirmessage_enable=YES
xferlog_enable=YES
connect_from_port_20=YES
xferlog_std_format=YES
listen=YES
pam_service_name=vsftpd
tcp_wrappers=YES
```

### 4. 创建配置文件

> 两个文件内容相同

```shell
vi /etc/vsftpd/chroot_list
${姓名缩写}
user_test

vi /etc/vsftpd/user_list
${姓名缩写}
user_test

# 重启ftp服务
service vsftpd restart
```

### 5. 验证效果 🍻

xxxxxxxxxx10 1nslookup2​3# 输入网址测试4# 正向解析5www.${姓名}-gz.com.cn6mail.${姓名}-gz.com.cn7​8# 反向解析9192.168.${学号}.2010192.168.${学号}.30shell

```shell
# 登录
ftp 192.168.${学号}.8

# 使用用户名：ftp
# 发现登录失败，说明匿名用户禁止登录
# 退出
quit

# 登录
ftp 192.168.${学号}.8

# 使用用户名：root
# 发现登录失败，说明匿名用户禁止登录
# 退出
quit
```

**`user_test`用户只能下载**

```shell
# 登录
ftp 192.168.${学号}.8

# 使用用户名：user_test
# 读文件
ls

# 下载文件到本地
get hello_zs.txt C:\\hello_zs.txt

# 创建目录
mkdir newDir
# 发现创建失败，说明 user_test 不能修改目录中的内容

# 改变所登录的目录
pwd
cd ../
# 发现修改失败，说明 user_test 不能改变所登录的目录
pwd
# 退出
quit
```

**`${姓名缩写}`对该目录有全部的控制权**

```shell
# 登录
ftp 192.168.${学号}.8

# 使用用户名：${姓名缩写}
# 读文件
ls

# 下载文件到本地
get hello_zs.txt C:\\hello_zs.txt

# 创建目录
mkdir newDir
# 发现创建成功，说明 ${姓名缩写} 对该目录有全部的控制权

# 改变所登录的目录
pwd
cd ../
# 发现修改失败，说明 ${姓名缩写} 不能改变所登录的目录
pwd
# 退出
quit
```