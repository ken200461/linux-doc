# 搭建DNS服务器

## 课题练习

**由Linux服务器负责维护一个正向区域：`zyc${学号}.com`，包含WWW主机、DNS主机、EMail主机、FTP主机。**

1. DNS主机解析到这台Linux服务器本身的地址：`192.168.${学号}.149`
2. WWW主机和FTP主机解析到：`192.168.${学号}.148`
3. EMail主机解析到：`192.168.${学号}.147`
4. 保证以上IP地址均能解析到正确的主机

### 1. 修改/添加配置文件

* 先把本机静态IP修改为：`192.168.${学号}.149`

> 初始化配置文件

```shell
cd /usr/share/doc/bind-9.3.3/sample
cp etc/*.* /var/named/chroot/etc
cp var/named/*.* /var/named/chroot/var/named
```

> 修改 /var/named/chroot/etc/named.conf

```shell
options
{
	query-source    port 53;	
	query-source-v6 port 53;

	directory "/var/named";
	dump-file 		"data/cache_dump.db";
        statistics-file 	"data/named_stats.txt";
        memstatistics-file 	"data/named_mem_stats.txt";

};
include "/etc/named.rfc1912.zones";
```

> 修改 /var/named/chroot/etc/named.rfc1912.zones

```shell
zone "localdomain" IN {
	type master;
	file "localdomain.zone";
	allow-update { none; };
};
zone "localhost" IN {
	type master;
	file "localhost.zone";
	allow-update { none; };
};
zone "0.0.127.in-addr.arpa" IN {
	type master;
	file "named.local";
	allow-update { none; };
};
zone "zyc${学号}.com" IN {
    type master;
    file "zyc${学号}.com.zone";
    allow-update { none; };
};
zone "${学号}.168.192.in-addr.arpa" IN {
    type master;
    file "zyc${学号}.com.arp";
    allow-update { none; };
};

zone
"0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa" IN {
        type master;
	file "named.ip6.local";
	allow-update { none; };
};

zone "255.in-addr.arpa" IN {
	type master;
	file "named.broadcast";
	allow-update { none; };
};

zone "0.in-addr.arpa" IN {
	type master;
	file "named.zero";
	allow-update { none; };
};
```

> 添加 /var/named/chroot/var/named/zyc${学号}.com.zone

```shell
$TTL	86400
@		IN SOA	@       root (
					42		; serial (d. adams)
					3H		; refresh
					15M		; retry
					1W		; expiry
					1D )		; minimum

@	            IN NS			dns.zyc${学号}.com.
@               IN MX   	1  	mail.zyc${学号}.com.
dns.zyc${学号}.com.     IN A        	192.168.${学号}.149
www.zyc${学号}.com.     IN A        	192.168.${学号}.148
ftp             IN CNAME   	 	www
mail.zyc${学号}.com.    IN A        	192.168.${学号}.147
```

> 添加 /var/named/chroot/var/named/zyc${学号}.com.arp

```shell
$TTL	86400
@       IN      SOA     localhost. root.localhost.  (
                                      1997022700 ; Serial
                                      28800      ; Refresh
                                      14400      ; Retry
                                      3600000    ; Expire
                                      86400 )    ; Minimum
@       IN      NS      dns.zyc${学号}.com.
149.${学号}.168.192.in-addr.arpa. IN PTR dns.zyc${学号}.com.
148     IN PTR  www.zyc${学号}.com.
147     IN PTR  mail.zyc${学号}.com.
```

### 2. 修改本机DNS为自己

> DNS配置文件：/etc/resolv.conf

```shell
# 在配置文件中添加
domain dns.zyc${学号}.com
nameserver 192.168.${学号}.149

# 重启服务
service named restart
```

### 3. 验证效果 🍻

> 必须在网络属性中选择使用 `192.168.${学号}.149` 为DNS服务器

```shell
nslookup

# 输入网址测试
# 正向解析
www.zyc${学号}.com
mail.zyc${学号}.com
ftp.zyc${学号}.com

# 反向解析
192.168.${学号}.148
192.168.${学号}.149
```

## 拓展练习

**设置 DNS 服务器的 IP 地址为 192.168.${学号}.40，子网掩码为 255.255.255.0。**
**另有一台客户端，地址为 192.168.${学号}.50。要求在客户端上验证：**

1. `dns.${姓名}-gz.com.cn` 为`192.168.${学号}.40`（本机）
2. `www.${姓名}-gz.com.cn` 为`192.168.${学号}.20`
3. `mail.${姓名}-gz.com.cn` 为`192.168.${学号}.30`
4. 能够将上述IP地址进行反向解析

### 1. 修改/添加配置文件

* 修改本机静态IP为`192.168.${学号}.40`

> 修改 /var/named/chroot/etc/named.rfc1912.zones

```shell
zone "localdomain" IN {
	type master;
	file "localdomain.zone";
	allow-update { none; };
};
zone "localhost" IN {
	type master;
	file "localhost.zone";
	allow-update { none; };
};
zone "0.0.127.in-addr.arpa" IN {
	type master;
	file "named.local";
	allow-update { none; };
};
zone "${姓名}-gz.com.cn" IN {
    type master;
    file "${姓名}-gz.com.cn.zone";
    allow-update { none; };
};
zone "${学号}.168.192.in-addr.arpa" IN {
    type master;
    file "${姓名}-gz.com.cn.arp";
    allow-update { none; };
};

zone
"0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa" IN {
        type master;
	file "named.ip6.local";
	allow-update { none; };
};

zone "255.in-addr.arpa" IN {
	type master;
	file "named.broadcast";
	allow-update { none; };
};

zone "0.in-addr.arpa" IN {
	type master;
	file "named.zero";
	allow-update { none; };
};
```

> 添加 /var/named/chroot/var/named/${姓名}-gz.com.cn.zone

```shell
$TTL	86400
@		IN SOA	@       root (
					42		; serial (d. adams)
					3H		; refresh
					15M		; retry
					1W		; expiry
					1D )		; minimum

@	            IN NS			dns.${姓名}-gz.com.cn.
@               IN MX   	1  	mail.${姓名}-gz.com.cn.
dns.${姓名}-gz.com.cn.     IN A        	192.168.${学号}.40
www.${姓名}-gz.com.cn.     IN A        	192.168.${学号}.20
ftp             IN CNAME   	 	www
mail.${姓名}-gz.com.cn.    IN A        	192.168.${学号}.30
```

> 添加 /var/named/chroot/var/named/${姓名}-gz.com.cn.arp

```shell
$TTL	86400
@       IN      SOA     localhost. root.localhost.  (
                                      1997022700 ; Serial
                                      28800      ; Refresh
                                      14400      ; Retry
                                      3600000    ; Expire
                                      86400 )    ; Minimum
@       IN      NS      dns.${姓名}-gz.com.cn.
40.${学号}.168.192.in-addr.arpa. IN PTR dns.${姓名}-gz.com.cn.
20     IN PTR  www.${姓名}-gz.com.cn.
30    IN PTR  mail.${姓名}-gz.com.cn.
1       IN      PTR     localhost.
```

### 2. 修改本机DNS为自己

> DNS配置文件：/etc/resolv.conf

```shell
# 在配置文件中添加
domain dns.${姓名}-gz.com.cn
nameserver 192.168.${学号}.40

# 重启服务
service named restart
```

### 3. 验证效果 🍻

> 必须在网络属性中选择使用 `192.168.${学号}.40` 为DNS服务器

```shell
nslookup

# 输入网址测试
# 正向解析
www.${姓名}-gz.com.cn
mail.${姓名}-gz.com.cn

# 反向解析
192.168.${学号}.20
192.168.${学号}.30
```