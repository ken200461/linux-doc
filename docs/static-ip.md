# 配置静态IP地址

## 1. 修改 VMware 网络配置

![VMware](../img/static_ip/vmware.png)

## 2. 修改 Linux 配置文件

* BOOTPROTO 改为 none
* IPADDR 改为 `192.168.${学号}.10`
* GATEWAY 改为 `192.168.${学号}.254`

```shell
# /etc/sysconfig/network-scripts/ifcfg-eth0
DEVICE=eth0
ONBOOT=yes
BOOTPROTO=none
HWADDR=00:0c:29:39:c8:08
IPADDR=192.168.${学号}.10
GATEWAY=192.168.${学号}.254

# 修改完后重启网卡
service network restart
```

## 3. 修改 Windows 配置

![Windows](../img/static_ip/windows.png)