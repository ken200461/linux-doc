# 搭建WEB服务器

## 课堂练习

**公司在一台Linux主机`192.168.${学号}.155`上建设了自己的Web网站。**
**为节省服务器资源，希望在此服务器上再为销售部建立一个Web网站，与现有网站共用一个IP地址与端口号（80）。**
**公司对于销售部网站的要求如下：**

1. 使用域名`www.sales${学号}.company.cn`访问
2. 网站资源在`/var/www/sales_${学号}`目录下，主页采用`index.html`
3. 管理员邮箱地址为`root@company.cn`
4. 网页编码为`GB2312`
5. 除销售部经理外，销售部每个员工都在该网站下开放自己的个人主页

### 1. 搭建DNS服务器

* 先把本机静态IP修改为：`192.168.${学号}.155`

> 初始化配置文件

```shell
cd /usr/share/doc/bind-9.3.3/sample
cp etc/*.* /var/named/chroot/etc
cp var/named/*.* /var/named/chroot/var/named
```

> 修改 /var/named/chroot/etc/named.conf

```shell
options
{
	query-source    port 53;	
	query-source-v6 port 53;

	directory "/var/named";
	dump-file 		"data/cache_dump.db";
        statistics-file 	"data/named_stats.txt";
        memstatistics-file 	"data/named_mem_stats.txt";

};
include "/etc/named.rfc1912.zones";
```

> 修改 /var/named/chroot/etc/named.rfc1912.zones

```shell
zone "localdomain" IN {
	type master;
	file "localdomain.zone";
	allow-update { none; };
};
zone "localhost" IN {
	type master;
	file "localhost.zone";
	allow-update { none; };
};
zone "0.0.127.in-addr.arpa" IN {
	type master;
	file "named.local";
	allow-update { none; };
};
zone "sales${学号}.company.cn" IN {
    type master;
    file "sales${学号}.company.cn.zone";
    allow-update { none; };
};
zone "${学号}.168.192.in-addr.arpa" IN {
    type master;
    file "sales${学号}.company.cn.arp";
    allow-update { none; };
};

zone
"0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa" IN {
        type master;
	file "named.ip6.local";
	allow-update { none; };
};

zone "255.in-addr.arpa" IN {
	type master;
	file "named.broadcast";
	allow-update { none; };
};

zone "0.in-addr.arpa" IN {
	type master;
	file "named.zero";
	allow-update { none; };
};
```

> 添加 /var/named/chroot/var/named/sales${学号}.company.cn.zone

```shell
$TTL	86400
@		IN SOA	@       root (
					42		; serial (d. adams)
					3H		; refresh
					15M		; retry
					1W		; expiry
					1D )		; minimum

@	            IN NS			www.sales${学号}.company.cn.
www.sales${学号}.company.cn.    IN A        	192.168.${学号}.155
```

> 添加 /var/named/chroot/var/named/sales${学号}.company.cn.arp

```shell
$TTL	86400
@       IN      SOA     localhost. root.localhost.  (
                                      1997022700 ; Serial
                                      28800      ; Refresh
                                      14400      ; Retry
                                      3600000    ; Expire
                                      86400 )    ; Minimum
@       IN      NS      www.sales${学号}.company.cn.
155.${学号}.168.192.in-addr.arpa. IN PTR www.sales${学号}.company.cn.
```

> DNS配置文件：/etc/resolv.conf

```shell
# 在配置文件中添加
domain www.sales${学号}.company.cn
nameserver 192.168.${学号}.155

# 重启服务
service named restart
```

### 2. 验证域名是否配置成功

```shell
nslookup
www.sales${学号}.company.cn
```

### 3. 创建用户

```shell
useradd -s /sbin/nologin sales_manager
passwd sales_manager
useradd -s /sbin/nologin saler_${姓名}
passwd saler_${姓名}
```

### 4. 创建目录并修改权限

```shell
mkdir /var/www/sales_${学号}
chmod 755 /var/www/sales_${学号}
vi /var/www/sales_${学号}/index.html

# 创建用户的根目录
mkdir /home/sales_manager/public_html
chmod 755 /home/sales_manager/public_html
mkdir /home/saler_${姓名}/public_html
chmod 755 /home/saler_${姓名}/public_html

vi /home/sales_manager/public_html/index.html
vi /home/saler_${姓名}/public_html/index.html
```

> /var/www/sales_${学号}/index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>销售部主页</title>
</head>
<body>
<h1>This is home page!</h1>
</body>
</html>
```

> /home/sales_manager/public_html/index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>销售部经理</title>
</head>
<body>
<h1>This is sales_manager's page!</h1>
</body>
</html>
```

> /home/saler_${姓名}/public_html/index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${姓名}</title>
</head>
<body>
<h1>This is saler_${姓名}'s page!</h1>
</body>
</html>
```

### 5. 修改/添加配置文件

> /etc/httpd/conf/httpd.conf

* 重启http服务：`service httpd restart`

```shell
ServerTokens OS
ServerRoot "/etc/httpd"
PidFile run/httpd.pid
Timeout 120
KeepAlive Off
MaxKeepAliveRequests 100
KeepAliveTimeout 15

<IfModule prefork.c>
StartServers       8
MinSpareServers    5
MaxSpareServers   20
ServerLimit      256
MaxClients       256
MaxRequestsPerChild  4000
</IfModule>

<IfModule worker.c>
StartServers         2
MaxClients         150
MinSpareThreads     25
MaxSpareThreads     75 
ThreadsPerChild     25
MaxRequestsPerChild  0
</IfModule>

Listen 80 
listen 8080

LoadModule auth_basic_module modules/mod_auth_basic.so
LoadModule auth_digest_module modules/mod_auth_digest.so
LoadModule authn_file_module modules/mod_authn_file.so
LoadModule authn_alias_module modules/mod_authn_alias.so
LoadModule authn_anon_module modules/mod_authn_anon.so
LoadModule authn_dbm_module modules/mod_authn_dbm.so
LoadModule authn_default_module modules/mod_authn_default.so
LoadModule authz_host_module modules/mod_authz_host.so
LoadModule authz_user_module modules/mod_authz_user.so
LoadModule authz_owner_module modules/mod_authz_owner.so
LoadModule authz_groupfile_module modules/mod_authz_groupfile.so
LoadModule authz_dbm_module modules/mod_authz_dbm.so
LoadModule authz_default_module modules/mod_authz_default.so
LoadModule ldap_module modules/mod_ldap.so
LoadModule authnz_ldap_module modules/mod_authnz_ldap.so
LoadModule include_module modules/mod_include.so
LoadModule log_config_module modules/mod_log_config.so
LoadModule logio_module modules/mod_logio.so
LoadModule env_module modules/mod_env.so
LoadModule ext_filter_module modules/mod_ext_filter.so
LoadModule mime_magic_module modules/mod_mime_magic.so
LoadModule expires_module modules/mod_expires.so
LoadModule deflate_module modules/mod_deflate.so
LoadModule headers_module modules/mod_headers.so
LoadModule usertrack_module modules/mod_usertrack.so
LoadModule setenvif_module modules/mod_setenvif.so
LoadModule mime_module modules/mod_mime.so
LoadModule dav_module modules/mod_dav.so
LoadModule status_module modules/mod_status.so
LoadModule autoindex_module modules/mod_autoindex.so
LoadModule info_module modules/mod_info.so
LoadModule dav_fs_module modules/mod_dav_fs.so
LoadModule vhost_alias_module modules/mod_vhost_alias.so
LoadModule negotiation_module modules/mod_negotiation.so
LoadModule dir_module modules/mod_dir.so
LoadModule actions_module modules/mod_actions.so
LoadModule speling_module modules/mod_speling.so
LoadModule userdir_module modules/mod_userdir.so
LoadModule alias_module modules/mod_alias.so
LoadModule rewrite_module modules/mod_rewrite.so
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_balancer_module modules/mod_proxy_balancer.so
LoadModule proxy_ftp_module modules/mod_proxy_ftp.so
LoadModule proxy_http_module modules/mod_proxy_http.so
LoadModule proxy_connect_module modules/mod_proxy_connect.so
LoadModule cache_module modules/mod_cache.so
LoadModule suexec_module modules/mod_suexec.so
LoadModule disk_cache_module modules/mod_disk_cache.so
LoadModule file_cache_module modules/mod_file_cache.so
LoadModule mem_cache_module modules/mod_mem_cache.so
LoadModule cgi_module modules/mod_cgi.so

Include conf.d/*.conf

User apache
Group apache

ServerAdmin root@localhost

UseCanonicalName Off

DocumentRoot "/var/www/html"

<Directory />
    Options FollowSymLinks
    AllowOverride None
</Directory>

<Directory "/var/www/html">

    Options Indexes FollowSymLinks

    AllowOverride None

    Order allow,deny
    Allow from all
</Directory>

<IfModule mod_userdir.c>
    UserDir disable sales_manager
    UserDir public_html
</IfModule>

DirectoryIndex index.html index.html.var

AccessFileName .htaccess

<Files ~ "^\.ht">
    Order allow,deny
    Deny from all
</Files>

TypesConfig /etc/mime.types

DefaultType text/plain

<IfModule mod_mime_magic.c>
    MIMEMagicFile conf/magic
</IfModule>

HostnameLookups Off

ErrorLog logs/error_log

LogLevel warn

LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogFormat "%h %l %u %t \"%r\" %>s %b" common
LogFormat "%{Referer}i -> %U" referer
LogFormat "%{User-agent}i" agent

Alias /icons/ "/var/www/icons/"

<Directory "/var/www/icons">
    Options Indexes MultiViews
    AllowOverride None
    Order allow,deny
    Allow from all
</Directory>

<IfModule mod_dav_fs.c>
    DAVLockDB /var/lib/dav/lockdb
</IfModule>

<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Order allow,deny
    Allow from all
</Directory>

AddIconByEncoding (CMP,/icons/compressed.gif) x-compress x-gzip

AddIconByType (TXT,/icons/text.gif) text/*
AddIconByType (IMG,/icons/image2.gif) image/*
AddIconByType (SND,/icons/sound2.gif) audio/*
AddIconByType (VID,/icons/movie.gif) video/*

AddIcon /icons/binary.gif .bin .exe
AddIcon /icons/binhex.gif .hqx
AddIcon /icons/tar.gif .tar
AddIcon /icons/world2.gif .wrl .wrl.gz .vrml .vrm .iv
AddIcon /icons/compressed.gif .Z .z .tgz .gz .zip
AddIcon /icons/a.gif .ps .ai .eps
AddIcon /icons/layout.gif .html .shtml .htm .pdf
AddIcon /icons/text.gif .txt
AddIcon /icons/c.gif .c
AddIcon /icons/p.gif .pl .py
AddIcon /icons/f.gif .for
AddIcon /icons/dvi.gif .dvi
AddIcon /icons/uuencoded.gif .uu
AddIcon /icons/script.gif .conf .sh .shar .csh .ksh .tcl
AddIcon /icons/tex.gif .tex
AddIcon /icons/bomb.gif core

AddIcon /icons/back.gif ..
AddIcon /icons/hand.right.gif README
AddIcon /icons/folder.gif ^^DIRECTORY^^
AddIcon /icons/blank.gif ^^BLANKICON^^

AddLanguage ca .ca
AddLanguage cs .cz .cs
AddLanguage da .dk
AddLanguage de .de
AddLanguage el .el
AddLanguage en .en
AddLanguage eo .eo
AddLanguage es .es
AddLanguage et .et
AddLanguage fr .fr
AddLanguage he .he
AddLanguage hr .hr
AddLanguage it .it
AddLanguage ja .ja
AddLanguage ko .ko
AddLanguage ltz .ltz
AddLanguage nl .nl
AddLanguage nn .nn
AddLanguage no .no
AddLanguage pl .po
AddLanguage pt .pt
AddLanguage pt-BR .pt-br
AddLanguage ru .ru
AddLanguage sv .sv
AddLanguage zh-CN .zh-cn
AddLanguage zh-TW .zh-tw

LanguagePriority en ca cs da de el eo es et fr he hr it ja ko ltz nl nn no pl pt pt-BR ru sv zh-CN zh-TW
ForceLanguagePriority Prefer Fallback

AddDefaultCharset UTF-8

AddType application/x-compress .Z
AddType application/x-gzip .gz .tgz

AddHandler type-map var

AddType text/html .shtml
AddOutputFilter INCLUDES .shtml

Alias /error/ "/var/www/error/"

<IfModule mod_negotiation.c>
<IfModule mod_include.c>
    <Directory "/var/www/error">
        AllowOverride None
        Options IncludesNoExec
        AddOutputFilter Includes html
        AddHandler type-map var
        Order allow,deny
        Allow from all
        LanguagePriority en es de fr
        ForceLanguagePriority Prefer Fallback
    </Directory>

</IfModule>
</IfModule>

BrowserMatch "Mozilla/2" nokeepalive
BrowserMatch "MSIE 4\.0b2;" nokeepalive downgrade-1.0 force-response-1.0
BrowserMatch "RealPlayer 4\.0" force-response-1.0
BrowserMatch "Java/1\.0" force-response-1.0
BrowserMatch "JDK/1\.0" force-response-1.0

BrowserMatch "Microsoft Data Access Internet Publishing Provider" redirect-carefully
BrowserMatch "MS FrontPage" redirect-carefully
BrowserMatch "^WebDrive" redirect-carefully
BrowserMatch "^WebDAVFS/1.[0123]" redirect-carefully
BrowserMatch "^gnome-vfs/1.0" redirect-carefully
BrowserMatch "^XML Spy" redirect-carefully
BrowserMatch "^Dreamweaver-WebDAV-SCM1" redirect-carefully

NameVirtualHost 192.168.${学号}.155:80

<VirtualHost 192.168.${学号}.155:80>
    ServerAdmin root@company.cn
    DocumentRoot /var/www/sales_${学号}
    ServerName www.sales${学号}.company.cn
    DirectoryIndex index.html
    ErrorLog logs/dummy-www.sales${学号}.company.cn-error_logs
    CustomLog logs/dummy-www.sales${学号}.company.cn-access_log common
</VirtualHost>
```

### 6. 验证效果 🍻

```url
# 成功访问
http://www.sales${学号}.company.cn
http://www.sales${学号}.company.cn/~saler_${姓名}
# 访问失败
http://www.sales${学号}.company.cn/~sales_manager
```

## 拓展练习

**在一台Linux网站服务器上建立两个虚拟站点，端口分别为`7000`和`9000`，域名为`www.${姓名缩写}-IT.com`。**
**站点根目录分别为`/var/www/${姓名缩写}1`和`/var/www/${姓名缩写}2`，存放不同网页，并通过域名分别进行访问：**

1. `http://www.${姓名缩写}-IT.com:7000`
2. `http://www.${姓名缩写}-IT.com:9000`

### 1. 创建目录及文件

```shell
mkdir /var/www/${姓名缩写}1
vi /var/www/${姓名缩写}1/index.html
mkdir /var/www/${姓名缩写}2
vi /var/www/${姓名缩写}2/index.html
```

> xxxxxxxxxx nslookup​# 输入网址测试# 正向解析www.${姓名}-gz.com.cnmail.${姓名}-gz.com.cn​# 反向解析192.168.${学号}.20192.168.${学号}.30shell

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${姓名缩写}1</title>
</head>
<body>
<h1>http://www.${姓名缩写}-IT.com:7000</h1>
</body>
</html>
```

> /var/www/${姓名缩写}2/index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${姓名缩写}2</title>
</head>
<body>
<h1>http://www.${姓名缩写}-IT.com:9000</h1>
</body>
</html>
```

### 2. 搭建DNS服务器

> 初始化配置文件

```shell
cd /usr/share/doc/bind-9.3.3/sample
cp etc/*.* /var/named/chroot/etc
cp var/named/*.* /var/named/chroot/var/named
```

> 修改 /var/named/chroot/etc/named.conf

```shell
options
{
	query-source    port 53;	
	query-source-v6 port 53;

	directory "/var/named";
	dump-file 		"data/cache_dump.db";
        statistics-file 	"data/named_stats.txt";
        memstatistics-file 	"data/named_mem_stats.txt";

};
include "/etc/named.rfc1912.zones";
```

> 修改 /var/named/chroot/etc/named.rfc1912.zones

```shell
zone "localdomain" IN {
	type master;
	file "localdomain.zone";
	allow-update { none; };
};
zone "localhost" IN {
	type master;
	file "localhost.zone";
	allow-update { none; };
};
zone "0.0.127.in-addr.arpa" IN {
	type master;
	file "named.local";
	allow-update { none; };
};
zone "${姓名缩写}-IT.com" IN {
    type master;
    file "${姓名缩写}-IT.com.zone";
    allow-update { none; };
};
zone "${学号}.168.192.in-addr.arpa" IN {
    type master;
    file "${姓名缩写}-IT.com.arp";
    allow-update { none; };
};

zone
"0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa" IN {
        type master;
	file "named.ip6.local";
	allow-update { none; };
};

zone "255.in-addr.arpa" IN {
	type master;
	file "named.broadcast";
	allow-update { none; };
};

zone "0.in-addr.arpa" IN {
	type master;
	file "named.zero";
	allow-update { none; };
};
```

> 添加 /var/named/chroot/var/named/${姓名缩写}-IT.com.zone

```shell
$TTL	86400
@		IN SOA	@       root (
					42		; serial (d. adams)
					3H		; refresh
					15M		; retry
					1W		; expiry
					1D )		; minimum

@	            IN NS			www.${姓名缩写}-IT.com.
www.${姓名缩写}-IT.com.    IN A        	192.168.${学号}.155
```

> 添加 /var/named/chroot/var/named/${姓名缩写}-IT.com.arp

```shell
$TTL	86400
@       IN      SOA     localhost. root.localhost.  (
                                      1997022700 ; Serial
                                      28800      ; Refresh
                                      14400      ; Retry
                                      3600000    ; Expire
                                      86400 )    ; Minimum
@       IN      NS      www.${姓名缩写}-IT.com.
155.${学号}.168.192.in-addr.arpa. IN PTR www.${姓名缩写}-IT.com.
```

> DNS配置文件：/etc/resolv.conf

```shell
# 在配置文件中添加
domain www.${姓名缩写}-IT.com
nameserver 192.168.${学号}.155

# 重启服务
service named restart
```

### 3. 验证域名是否配置成功

```shell
nslookup
www.${姓名缩写}-IT.com
```

### 4. 修改/添加配置文件

> /etc/httpd/conf/httpd.conf

**在此注释下添加（删除其余`VirtualHost`标签）**

```shell
# VirtualHost example:
# Almost any Apache directive may go into a VirtualHost container.
# The first VirtualHost section is used for requests without a known
# server name.
```

```shell
<VirtualHost 192.168.${学号}.155:7000>
    DocumentRoot /var/www/${姓名缩写}1
    ServerName www.${姓名缩写}-IT.com
    DirectoryIndex index.html
</VirtualHost>

<VirtualHost 192.168.${学号}.155:9000>
    DocumentRoot /var/www/${姓名缩写}2
    ServerName www.${姓名缩写}-IT.com
    DirectoryIndex index.html
</VirtualHost>
```

**修改完毕后重启服务：`service httpd restart`**

### 5. 验证效果🍻

```url
# 成功访问
http://www.${姓名缩写}-IT.com:7000
http://www.${姓名缩写}-IT.com:9000
```