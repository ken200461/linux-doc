# 搭建Samba服务器

## 课堂练习

**公司要在工作组company中添加一台Samba服务器作为文件服务器。**
**把需要公开的信息发布在一个名为public的共享目录/share中。**
**为实现集中管理，还要为公司各部门建立相应的目录。**
**比如销售部的资料存放在Samba服务器的/cmpdata/sales目录下，要求只允许销售部员工和总经理访问，并且只允许销售部经理对数据进行维护。**

![Samba](../img/samba/samba.png)

### 1. 创建用户组

```shell
groupadd sales
```

### 2. 创建系统用户

* -g 添加主组<br/>
* -G 添加附加组

```shell
useradd -g sales saler_a
useradd -g sales saler_b
# 销售部经理
useradd -g sales dm_${姓名}
# 总经理
useradd -G sales gm_${姓名}
# 其他部门员工
useradd test
```

### 3. 添加Samba用户并设置密码

* 此密码与设置登录密码不同

```shell
smbpasswd -a saler_a
smbpasswd -a saler_b
smbpasswd -a dm_${姓名}
smbpasswd -a gm_${姓名}
smbpasswd -a test
```

### 4. 创建文件夹

* 创建文件：`touch`
* 递归创建目录：`mkdir -p`

```shell
mkdir -p /samba/share
cd /samba/share
touch shareFile.txt
cd /samba
mkdir -p ./cmpdata/sales
touch salesData.txt
```

### 5. 设置权限

> /cmpdata/sales

* ls -l 查看权限
* 修改目录权限：`chown ${目录所有者}:${组名} ${目录名}`
    * 目录所有者 rwx (读 写 执行)
    * 同组人员 r-x
    * 组外 r-x

```shell
# 设置组外人员不能访问
chmod 750 /samba/cmpdata/sales
chown dm_${姓名}:sales /samba/cmpdata/sales
cd /samba/cmpdata/sales
touch salesFile.txt
```

### 6. 修改 Samba 配置文件

> 完整的配置文件

* 修改后需要重启：`service smb restart`

```shell
# /etc/samba/smb.conf
[global]
   workgroup = MYGROUP
   server string = Samba Server
   security = user
   map to guest = bad user
   load printers = yes
   cups options = raw
   log file = /var/log/samba/%m.log
   max log size = 50
   dns proxy = no

#============================ Share Definitions ==============================
[homes]
   comment = Home Directories
   browseable = no
   writable = yes

[printers]
   comment = All Printers
   path = /usr/spool/samba
   browseable = no
   guest ok = no
   writable = no
   printable = yes

[public]
    path = /samba/share
    guest ok = yes
    writable = no
    printable = no

[sales]
    path = /samba/cmpdata/sales
    valid users = @sales
    write list = dm_${姓名}
    # 不能无登录访问
    public = no
    printable = no
```

## 拓展练习

**Linux作为服务器，IP地址是`192.168.${学号}.210`，服务器上有一个目录`/hello_${姓名}`，里面包含`share_${姓名}`文件。**
**实现`/hello_${姓名}`文件夹的只读共享，客户机分别是windows和Linux,给出过程和验证结果截图。**
**在Linux服务器上创建一个`/home/samba`目录，目录里有一个`welcome_${姓名}`文件。**
**只允许`192.168.${学号}.8`的主机去访问，只允许某组用户（组名为`${姓名缩写}`）对其维护（读写），其他用户（如`other`）只能访问，不能写入。**
**给出过程和验证结果截图，包括能正常访问和访问被拒绝。客户机分别是windows和Linux,给出过程和验证结果截图。**

### 1. 修改静态 IP

```shell
# /etc/sysconfig/network-scripts/ifcfg-eth0
DEVICE=eth0
ONBOOT=yes
BOOTPROTO=none
HWADDR=00:0c:29:39:c8:08
# 修改此行
IPADDR=192.168.40.210
GATEWAY=192.168.40.254

# 修改完后重启网卡
service network restart
```

### 2. 创建用户组以及用户

```shell
# 添加用户组
groupadd ${姓名缩写}

# 添加用户到用户组
useradd -g ${姓名缩写} allowUser

# 无权限访问的用户
useradd otherUser

# 添加 Samba 用户并设置密码
smbpasswd -a allowUser
smbpasswd -a otherUser
```

### 3. 创建目录并设置权限

* `shown ${目录所有者}:${组名} ${目录名}`
* 所有者和组内成员可读可写：`chmod 770`

```shell
# 只读共享
mkdir /hello_${姓名}
touch /hello_${姓名}/share_${姓名}
# 组内共享
mkdir -p /home/samba
touch /home/samba/welcome_${姓名}
chmod 775 /home/samba
chown allowUser:nck /home/samba
```

### 4. 修改 Samba 配置文件

* 限制访问 IP 关键字：`hosts allow`
* 重启 Samba 服务：`service smb restart`

```shell
# /etc/samba/smb.conf
[global]
   workgroup = MYGROUP
   server string = Samba Server
   security = user
   map to guest = bad user
   load printers = yes
   cups options = raw
   log file = /var/log/samba/%m.log
   max log size = 50
   dns proxy = no

#============================ Share Definitions ==============================
[homes]
   comment = Home Directories
   browseable = no
   writable = yes

[printers]
   comment = All Printers
   path = /usr/spool/samba
   browseable = no
   guest ok = no
   writable = no
   printable = yes

[hello_${姓名}]
    path = /hello_${姓名}
    guest ok = yes
    writable = no
    printable = no

[home_samba]
    path = /home/samba
    public = no
    hosts deny = all
    hosts allow = 192.168.${学号}.8
    write list = @${姓名缩写}
    printable = no
```

### 5. 验证效果 🍻

#### Windows客户端

**公开只读的目录：`hello_${姓名}`** <br/><br/>
![公开只读的目录](../img/samba/extend/public_dir1.jpg)
![该目录下无法创建文件](../img/samba/extend/public_dir2.png) <br/><br/>
**`192.168.${学号}.8`的组内用户才能读写的目录：`home_samba`** <br/><br/>
![当前主机IP地址为192.168.40.8](../img/samba/extend/allowUser1.png)
![登录allowUser账号](../img/samba/extend/allowUser2.png)
![成功读写文件](../img/samba/extend/allowUser3.png) <br/><br/>
**其他用户无法访问目录：`home_samba`** <br/><br/>
![登录otherUser账号](../img/samba/extend/otherUser1.png)
![无法访问 home_samba 目录](../img/samba/extend/otherUser2.png)

#### Linux客户端

**公开只读的目录：`hello_${姓名}`** <br/><br/>
![公开只读的目录](../img/samba/extend/public_dir-linux.png) <br/><br/>
**`192.168.${学号}.8`的组内用户才能读写的目录：`home_samba`** <br/><br/>
![当前主机IP地址为192.168.40.8](../img/samba/extend/allowUser1-linux.png)
![成功读写文件](../img/samba/extend/allowUser2-linux.png) <br/><br/>
**IP不符合的用户无法访问目录：`home_samba`** <br/><br/>
![IP不符合的用户](../img/samba/extend/otherUser1-linux.png)
![无法访问 home_samba 目录](../img/samba/extend/otherUser2-linux.png)