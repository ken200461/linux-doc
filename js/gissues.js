let gissuesEle;

function getIframe() {
    if (gissuesEle) return;
    const iter = setInterval(() => {
        let gissuesElement = document.querySelector('.Gissues');
        if (!gissuesElement) return;
        let iframe = gissuesElement.children[0];
        if (!iframe) return;
        clearInterval(iter);
        gissuesElement.style.width = '92%';
        gissuesElement.style['max-width'] = 'none';
        iframe.loading = 'lazy';
        iframe.width = '100%';
        iframe.height = '100%';
        gissuesEle = gissuesElement;
        if (gissuesEle) gissuesEle.parentNode.removeChild(gissuesEle);
    }, 300);
}

function addIframe() {
    let articleElement = document.querySelector('article');
    let len = articleElement.children.length;
    const iter = setInterval(async () => {
        articleElement = document.querySelector('article');
        if (articleElement.children.length !== len) {
            len = articleElement.children.length;
            return;
        }
        clearInterval(iter);
        if (gissuesEle) articleElement.appendChild(gissuesEle);
    }, 200);
}

function parseParams(src, name, params) {
    name = name + '=';
    const oldVal = src.slice(src.indexOf(name), src.length)?.split('&')[0];
    return src.replace(oldVal, name + encodeURIComponent(params));
}

function getPathname() {
    let hash = window.location.hash?.split('/');
    return hash[hash.length - 1]?.split('?')[0].toUpperCase() || 'README';
}

function changeSrc() {
    const interval = setInterval(() => {
        if (!gissuesEle) return;
        clearInterval(interval);
        let ret = parseParams(gissuesEle.children[0].src, 'url', window.location.href);
        ret = parseParams(ret, 'origin', window.location.href);
        ret = parseParams(ret, 'issue-number', gissuesMap[getPathname()]);
        gissuesEle.children[0].src = ret;
    }, 200);
}

(function () {
    getIframe();
    window.addEventListener('load', () => {
        changeSrc();
        addIframe();
    });
    window.addEventListener("hashchange", () => {
        getIframe();
        changeSrc();
        addIframe();
    });
})();