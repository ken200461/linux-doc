const fn = () => {
    const bilibiliEleArr = document.querySelectorAll('iframe.Bilibili');
    for (let i = 0; i < bilibiliEleArr.length; i++) {
        const bilibiliEle = bilibiliEleArr[i];
        const width = bilibiliEle.clientWidth;
        const scale = bilibiliEle.getAttribute('scale')?.split(':') || [16, 9];
        const w = scale[0];
        const h = scale[1];
        bilibiliEle.height = width / w * h + 'px';
        bilibiliEle.scrolling = "no";
        bilibiliEle.border = "0";
        bilibiliEle.frameborder = "no";
        bilibiliEle.framespacing = "0";
    }
}

const bilibiliVideo = () => {
    const bilibiliEleArr = document.querySelectorAll('iframe.Bilibili');
    let num = 0;
    const iter = setInterval(() => {
        num++;
        if (bilibiliEleArr.length || num > 50) clearInterval(iter);
        fn();
    }, 10);
}

(function () {
    window.addEventListener('load', bilibiliVideo);
    window.addEventListener('resize', bilibiliVideo);
    window.addEventListener('hashchange', bilibiliVideo);
})()