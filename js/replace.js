toastr.options = {
    closeButton: true, //显示关闭按钮
    debug: false,
    progressBar: true, //显示进度条
    positionClass: "toast-top-right", //位置
    // onclick: null, //点击消息框自定义事件
    showDuration: "300", //显示动作时间
    hideDuration: "300", //隐藏动作时间
    timeOut: "3000", //显示时间,0为永久显示
    // extendedTimeOut: "1000", //鼠标移动过后显示显示时间
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn", //显示方式
    hideMethod: "fadeOut" //隐藏方式
};

let default_html;

function ask(message) {
    return new Promise(resolve => {
        UI.ask(message, val => {
            resolve(val);
        });
    });
}

function changeVar() {
    setTimeout(() => {
        const name = localStorage.getItem('name');
        const num = localStorage.getItem('num');
        if (!(name && num)) return;
        const nameSpell = name.spell().toLowerCase();
        const firstName = name.spell('first').toLowerCase();
        const articleEle = document.querySelector('article');
        if (!default_html) default_html = articleEle.innerHTML;
        articleEle.innerHTML = default_html.replaceAll('${姓名}', nameSpell)
            .replaceAll('${学号}', num)
            .replaceAll('${姓名缩写}', firstName);
    });
}

window.addEventListener('load', changeVar);
window.addEventListener('hashchange', () => {
    default_html = null;
    changeVar();
});

async function setVal() {
    const name = await ask('请输入你的姓名：');
    if (!(name && /^[\u4e00-\u9fa5]+(·[\u4e00-\u9fa5]+)*$/g.test(name))) {
        toastr.error('姓名必须为中文', '信息不合法');
        return;
    }
    const inter = setInterval(() => {
        document.querySelector('.UI_mask').style.display = 'block';
    }, 50);
    const num = await ask('请输入你的学号：');
    if (num && num !== '0' && /^[0-9]{1,2}$/g.test(num)) {
        localStorage.setItem('name', name);
        localStorage.setItem('num', num);
        changeVar();
        toastr.success("页面内容已替换", "添加成功");
        clearInterval(inter);
        return;
    }
    toastr.error('学号必须为两位以下数字', '信息不合法');
}

async function fillBtn() {
    let name = localStorage.getItem('name');
    let num = localStorage.getItem('num');
    if (name && num) {
        UI.confirm({
            text: `已存在信息！姓名：${name}，学号：${num}，是否覆盖？`,
            btns: ['覆盖', '取消'],
            async callback() {
                await setVal();
            }
        });
    } else {
        await setVal();
    }
}

function clearBtn() {
    const name = localStorage.getItem('name');
    const num = localStorage.getItem('num');
    if (!(name && num)) {
        toastr.warning("信息未填写");
        return;
    }
    localStorage.removeItem('name');
    localStorage.removeItem('num');
    if (!default_html) {
        toastr.warning("请手动刷新");
    } else {
        const articleEle = document.querySelector('article');
        articleEle.innerHTML = default_html;
    }
    setTimeout(() => {
        toastr.success("信息清除成功");
    }, 800);
}