# 💻 前言

> IDEA 下载地址：[IDEA 2021.2.2](https://download.jetbrains.com/idea/ideaIU-2021.2.2.exe?_gl=1*m73cuj*_ga*Njg0NDI5MjA1LjE2MjUxMDkwMTI.*_ga_9J976DJZ68*MTY2ODQ4MzAwNS4xOS4wLjE2Njg0ODMwMTIuMC4wLjA.&_ga=2.168140193.1210615591.1668483006-684429205.1625109012)

最好选用 **IDEA** `Community` 或者 `2021.2.2` 以下的版本，防止破解失败。

破解插件 `IDE Eval Reset` 第三方仓库地址：`https://plugins.zhile.io`

由于直接在 VMware 操作体验极差！有不能复制，虚拟机经常会死机导致配置文件丢失等等问题。

所以建议使用 `SSH` && `SFTP` 连接虚拟机后在集成开发环境**（IDEA）**中操作，可以保证在虚拟机死机后也能留住写到一半的配置文件。最重要的，配置文件还有代码提示和高亮！

[📺 BiliBili](https://www.bilibili.com) 外链视频的清晰度可能有所不足，建议前往官网观看。

高清视频地址：[https://www.bilibili.com/video/BV1oe41137sC](https://www.bilibili.com/video/BV1oe41137sC)

<iframe class="Bilibili" scale="16:9" allowfullscreen="true" src="//player.bilibili.com/player.html?aid=261967045&bvid=BV1oe41137sC&cid=870559427&page=1"></iframe>